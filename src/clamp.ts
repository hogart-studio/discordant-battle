export function clamp(a: number, b: number, val: number): number {
    return Math.max(Math.min(val, b), a);
}
