import {IPos} from '@/IPos';
import {clamp} from '@/clamp';
import {DamageType} from '@/Weapon';

export type EnemyName = 'small' | 'flying' | 'big';

export abstract class Enemy {
    public hp: number;
    public armor: number;
    public pos!: IPos;

    constructor(
        public readonly name: string,
        public readonly speed: number,
        public readonly maxHp: number,
        public readonly maxArmor: number = 0
    ) {
        this.hp = maxHp;
        this.armor = maxArmor;
    }

    get isDead(): boolean {
        return this.hp === 0;
    }

    damage(value: number, type: DamageType = 'pierce') {
        if (this.armor) {
            if (type === 'pierce') {
                return; // while armor is in place, 'pierce' damage has no effect
            } else if (type === 'blast') {
                if (value > this.armor) {
                    const hpDamage = value - this.armor;
                    this.armor = 0;
                    this.hp = clamp(0, this.maxHp, this.hp - hpDamage);
                } else if (value <= this.armor) {
                    this.armor = clamp(0, this.maxArmor, this.armor - value);
                }
            }
        } else {
            this.hp = clamp(0, this.maxHp, this.hp - value);
        }
    }

    advance(): void {
        this.pos.y += this.speed;
    }

    static create(name: EnemyName, pos: IPos = {x: 0, y: 0}): Enemy {
        if (name === 'small') {
            return new SmallEnemy(pos);
        } else if (name === 'flying') {
            return new FlyingEnemy(pos);
        } else {
            return new BigEnemy(pos);
        }
    }
}

export class SmallEnemy extends Enemy {
    constructor(pos: IPos) {
        super('«Паучок»', 2, 1);
        this.pos = pos;
    }
}

export class FlyingEnemy extends Enemy {
    constructor(pos: IPos) {
        super('«Шершень»', 3, 2);
        this.pos = pos;
    }
}

export class BigEnemy extends Enemy {
    constructor(pos: IPos) {
        super('М-500', 3, 6, 2);
        this.pos = pos;
    }
}
