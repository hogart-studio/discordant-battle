import {Weapon, WeaponName} from '@/Weapon';

export class Fighter {
    public weapons: Weapon[] = [];

    constructor(
        public readonly name: string,
        armedWith: WeaponName[]
    ) {
        this.weapons = armedWith.map(Weapon.create);
    }

    get hasTurns(): boolean {
        for (const weapon of this.weapons) {
            if (!weapon.hasFired) {
                return true;
            }
        }
        return false;
    }

    reload() {
        this.weapons.forEach((w) => w.hasFired = false);
    }
}
