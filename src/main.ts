import Vue from 'vue';
import App from './components/App.vue';

import './style.styl';
import {WeaponName} from '@/Weapon';

Vue.config.productionTip = false;

function renderBattle(enemiesList: string, fightersList: Record<string, WeaponName[]>, elementOrSelector: Element | string = '#app') {
    const props = {
        enemiesList,
        fightersList,
    };
    const app = new Vue({
        render(h){
            return h(App, {props});
        },
    });

    app.$mount(elementOrSelector);

    return app;
}

// @ts-ignore
window.renderBattle = renderBattle;


