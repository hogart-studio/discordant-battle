export type WeaponName = 'mg' | 'ar' | 'gl';
export type DamageType = 'blast' | 'pierce';

export abstract class Weapon {
    public hasFired: boolean = false;

    constructor(
        public readonly name: string,
        public readonly damage: number,
        public readonly size: number = 1,
        public readonly type: DamageType = 'pierce'
    ) {
    }

    static create(kind: WeaponName): Weapon {
        if (kind === 'mg') {
            return new MachineGunWeapon();
        } else if (kind === 'ar') {
            return new AssaultRifleWeapon();
        } else {
            return new UnderbarrelWeapon();
        }
    }
}

export class MachineGunWeapon extends Weapon {
    constructor() {
        super('Пулемёт', 4);
    }
}

export class AssaultRifleWeapon extends Weapon {
    constructor() {
        super('Автомат', 2);
    }
}

export class UnderbarrelWeapon extends Weapon {
    constructor() {
        super('Подствольник', 2, 2, 'blast');
    }
}
